#pragma once
#include <string>
#include <Windows.h>
#include <list>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/utime.h>
#include <time.h>
#include <chrono>
#include <cstdio>
#include <nlohmann/json.hpp>
#include <jipc/Message.h>

namespace JIPC
{
	using sysclock = std::chrono::system_clock;
	enum class IPCMode : int
	{
		SERVER,
		CLIENT
	};

	class FileStream
	{
	public:
		FileStream() : _Path(std::to_string(GetCurrentProcessId()) + ".ipc") {
			std::ofstream ofs(_Path);
			ofs << "[]";
			ofs.close();
		}

		FileStream(std::string path) : _Path(path) {}
		~FileStream() { std::remove(_Path.c_str()); }

		std::list<Message> PullMessages();
		void WriteMessages(std::list<Message>);

		std::string Path() { return _Path;}
	protected:
		std::string _Path;
		std::string _LockFile = _Path + ".lock";
		time_t lastUpdateTime = 0;

		void Lock();
		void Unlock();

		void SetMtime(time_t mtime);
		struct stat GetTimes();
	};

#pragma region Implementations
	std::list<Message> FileStream::PullMessages()
	{
		auto mtime = GetTimes().st_mtime;
		if (mtime <= lastUpdateTime)
			return {};

		lastUpdateTime = mtime;
		
		Lock();

		nlohmann::json jMessages;
		std::ifstream ifs(_Path);
		ifs >> jMessages;
		ifs.close();

		std::list<Message> messages;
		nlohmann::json other  = nlohmann::json::array();
		for(const auto &m : jMessages)
			if((int)m["sender"] != (int)GetCurrentProcessId())
				messages.push_back(Message(m));
			else
				other.push_back(m);
		
		std::ofstream ofs(_Path);
		ofs << other.dump();
		ofs.close();

		SetMtime(lastUpdateTime);

		Unlock();

		return messages;
	}

	void FileStream::WriteMessages(std::list<Message> messages)
	{
		Lock();
		lastUpdateTime = sysclock::to_time_t(sysclock::now()) + 1;

		nlohmann::json j;
		std::ifstream ifs(_Path);
		ifs >> j;
		ifs.close();

		for(auto &m : messages)
			j.push_back(m.ToJson());

		std::ofstream ofs(_Path);
		ofs << j.dump();
		ofs.close();

		SetMtime(lastUpdateTime);
		Unlock();
	}

	void FileStream::Lock()
	{
		struct stat test;
		while(stat(_LockFile.c_str(), &test) == 0)
			Sleep(10);
		
		std::ofstream {_LockFile.c_str()};
	}

	void FileStream::Unlock()
	{
		std::remove(_LockFile.c_str());
	}

	void FileStream::SetMtime(time_t mtime)
	{
		struct _utimbuf times;
		struct stat fstat;
		if (stat(_Path.c_str(), &fstat) != 0) {
			perror(_Path.c_str());
			return;
		}

		times.actime = fstat.st_atime;
		times.modtime = mtime;
		if (_utime(_Path.c_str(), &times) != 0) {
			perror(_Path.c_str());
			return;
		}
	}

	struct stat FileStream::GetTimes()
	{
		struct stat fstat;
		if (stat(_Path.c_str(), &fstat) != 0) {
			throw new std::exception("file error");
		}
		return fstat;
	}
#pragma endregion
}