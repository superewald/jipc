#pragma once
#include <functional>
#include <map>
#include <jipc/Thread.h>
#include <jipc/FileStream.h>

namespace JIPC
{
    class Connection
    {
        using Handler = std::function<void(Connection*, const Message&)>;
    public:
        Connection(FileStream& stream, std::map<std::string, Handler> handlers = {}) : _Stream(stream), _Handlers(handlers) { }

        void Send(Message m);
        void ProcessMessages();
    protected:
        FileStream _Stream;
        std::map<std::string, Handler> _Handlers;
        std::list<Message> _MessageCache = {};
    };

    void Connection::Send(Message m) { _MessageCache.push_back(m); }

    void Connection::ProcessMessages()
    {
        auto messages = _Stream.PullMessages();

        for(auto message : messages)
            if(_Handlers.find(message.URI()) != _Handlers.end())
                _Handlers[message.URI()](this, message);


        if (!_MessageCache.empty())
        {
            _Stream.WriteMessages(_MessageCache);
            _MessageCache.clear();
        }
    }
}
