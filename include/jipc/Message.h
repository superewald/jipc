#pragma once

#include <Windows.h>
#include <string>
#include <nlohmann/json.hpp>

namespace JIPC
{
	class Message
	{
	public:
		Message(std::string uri, nlohmann::json body) : _Sender(GetCurrentProcessId()), _URI(uri), _Body(body) {}
		Message(nlohmann::json j) : _Sender(j["sender"].get<int>()), _URI(j["uri"].get<std::string>()), _Body(j["body"]) {}

		int Sender() { return _Sender; }
		std::string URI() { return _URI; }

		nlohmann::json Body() { return _Body; }

		template<typename TBody>
		TBody Body() { return TBody(_Body); }

		nlohmann::json ToJson()
		{
			return {{"uri", _URI}, {"sender", _Sender}, {"body", _Body}};
		}
	protected:
		int _Sender;
		std::string _URI;
		nlohmann::json _Body;
	};
}