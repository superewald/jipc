#pragma once
#include <string>
#include <jipc/Message.h>

namespace JIPC
{
    class Connection;
    class Handler
    {
    public:
        virtual std::string URI() = 0;
        virtual void Process(Connection* conn, Message m) = 0;
    };
}