CPMAddPackage(
    NAME nlohmann_json
    VERSION 3.10.4
    URL https://github.com/nlohmann/json/releases/download/v3.10.4/include.zip
)